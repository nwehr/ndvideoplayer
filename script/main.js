//
//	Copyright 2011-2013 Nathan Wehr. All Rights Reserved. 
//
//	Redistribution and use in source and binary forms, with or without modification, are
//	permitted provided that the following conditions are met:
//
//		1. Redistributions of source code must retain the above copyright notice, this list of
//		conditions and the following disclaimer.
//
//		2. Redistributions in binary form must reproduce the above copyright notice, this list
//		of conditions and the following disclaimer in the documentation and/or other materials
//		provided with the distribution.
//
//	THIS SOFTWARE IS PROVIDED BY NATHAN WEHR ''AS IS'' AND ANY EXPRESS OR IMPLIED
//	WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
//	FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL NATHAN WEHR OR
//	CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
//	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
//	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
//	ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//	NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
//	ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//	The views and conclusions contained in the software and documentation are those of the
//	authors and should not be interpreted as representing official policies, either expressed
//	or implied, of Nathan Wehr.
//

var _PLAYER_DEFAULT_WIDTH_ = 700; 
var _PLAYER_DEFAULT_HEIGHT_ = 393;

var BufferProgressInterval = false; 
var PlayProgressInterval = false; 

var VideoPlayer; 

var ControllerOpacity = 1.0; 
var ControllerOpacityTimeout = false; 

////////////////////////////////////////////////////////////////////////////////////
// NDSize
////////////////////////////////////////////////////////////////////////////////////
function NDSize( i_Width, i_Height ){
	Object.defineProperties( this, {
		m_Width : {
			value: i_Width ? i_Width : 0
			, writable: true
		}
		, m_Height : {
			value: i_Height ? i_Height : 0
			, writable: true
		}
		
	}); 
	
}

NDSize.prototype.constructor = NDSize; 

NDSize.prototype.SetWidth = function( i_Width ){
	this.m_Width = i_Width ? i_Width : 0; 
}; 

NDSize.prototype.GetWidth = function(){
	return this.m_Width; 
}; 

NDSize.prototype.SetHeight = function( i_Height ){
	this.m_Height = i_Height ? i_Height : 0; 
}; 


NDSize.prototype.GetHeight = function(){
	return this.m_Height; 
}; 

////////////////////////////////////////////////////////////////////////////////////
// NDPosition
////////////////////////////////////////////////////////////////////////////////////
function NDPosition( i_X, i_Y ){
	Object.defineProperties( this, {
		m_X : {
			value: i_X ? i_X : 0
			, writable: true
		}
		, m_Y : {
			value: i_Y ? i_Y : 0
			, writable: true
		}
		
	}); 
	
}

NDPosition.prototype.constructor = NDPosition; 

NDPosition.prototype.SetX = function( i_X ){
	this.m_X = i_X ? i_X : 0;
}; 

NDPosition.prototype.GetX = function(){
	return this.m_X; 
}; 

NDPosition.prototype.SetY = function( i_Y ){
	this.m_Y = i_Y ? i_Y : 0; 
}; 

NDPosition.prototype.GetY = function(){
	return this.m_Y; 
}; 

////////////////////////////////////////////////////////////////////////////////////
// NDVideoSource
////////////////////////////////////////////////////////////////////////////////////
function NDVideoSource( i_Location, i_Type ){
	Object.defineProperties( this, {
		m_Location : {
			value: i_Location
			, writable: false
		}
		, m_Type : {
			value: i_Type
			, writable: false
		}
		
	}); 
	
}

NDVideoSource.prototype.constructor = NDVideoSource; 

NDVideoSource.prototype.GetLocation = function(){
	return this.m_Location; 
}; 

NDVideoSource.prototype.GetType = function(){
	return this.m_Type; 
}; 

////////////////////////////////////////////////////////////////////////////////////
// NDVolumeController
////////////////////////////////////////////////////////////////////////////////////
function NDVolumeController( i_VideoController ){
	Object.defineProperties( this, {
		m_VideoController : {
			value: i_VideoController
			, writable: true
		}
		, m_ContainerElement : {
			value: document.createElement( 'div' )
			, writable: true
		}
		, m_VolumeElement : {
			value: document.createElement( 'div' )
			, writable: true
		}
		, m_VolumePositionElement : {
			value: document.createElement( 'div' )
			, writable: true
		}
		, m_VolumeIndicatorElement : {
			value: document.createElement( 'img' )
			, writable: true
		}
		, m_Seeking : {
			value: false
			, writable: true
		}
		
	}); 
	
	this.m_VolumePositionElement.setAttribute( 'class', 'VolumePosition' );
	
	this.m_VolumeIndicatorElement.setAttribute( 'src', '/NDVideoPlayer/images/volume.png' );
	this.m_VolumeIndicatorElement.setAttribute( 'onclick', 'VideoPlayer.GetVideoController().GetVolumeController().SetMuted(true)' ); 
	
	this.m_VolumeElement.setAttribute( 'class', 'Volume' ); 
	this.m_VolumeElement.setAttribute( 'onmousedown', 'VideoPlayer.GetVideoController().GetVolumeController().StartSeek(event);' ); 
	this.m_VolumeElement.setAttribute( 'onmouseup', 'VideoPlayer.GetVideoController().GetVolumeController().EndSeek();' ); 
	this.m_VolumeElement.setAttribute( 'onmousemove', 'VideoPlayer.GetVideoController().GetVolumeController().TrackPointer(event);' ); 
	this.m_VolumeElement.appendChild( this.m_VolumePositionElement ); 
	
	this.m_ContainerElement.setAttribute( 'class', 'VolumeContainer' ); 
	this.m_ContainerElement.appendChild( this.m_VolumeIndicatorElement ); 
	this.m_ContainerElement.appendChild( this.m_VolumeElement ); 
	
}

NDVolumeController.prototype.constructor = NDVolumeController; 

NDVolumeController.prototype.GetContainerElement = function(){
	return this.m_ContainerElement; 
}; 

NDVolumeController.prototype.SetMuted = function( i_Muted ){
	this.m_VideoController.GetVideoPlayer().GetVideoElement().muted = i_Muted; 
	
	if( i_Muted ){
		this.m_VolumeIndicatorElement.setAttribute( 'src', '/NDVideoPlayer/images/mute.png' );
		this.m_VolumeIndicatorElement.setAttribute( 'onclick', 'VideoPlayer.GetVideoController().GetVolumeController().SetMuted(false)' ); 
		
	} else {
		this.m_VolumeIndicatorElement.setAttribute( 'src', '/NDVideoPlayer/images/volume.png' );
		this.m_VolumeIndicatorElement.setAttribute( 'onclick', 'VideoPlayer.GetVideoController().GetVolumeController().SetMuted(true)' ); 
	}
	
}; 

NDVolumeController.prototype.GetPointerPosition = function( el ){
	var X = 0; 
	var Y = 0; 
	
	while( el && !isNaN( el.offsetLeft ) ){
		X += el.offsetLeft - el.scrollLeft; 
		Y += el.offsetTop - el.scrollTop; 
		
		el = el.offsetParent; 
	}
	
	return new NDPosition( X, Y ); 
	
}; 

NDVolumeController.prototype.TrackPointer = function( event ){
	if( this.m_Seeking ){
		var Position = this.GetPointerPosition( this.m_VolumeElement ); 
		
		if( (event.clientX - Position.GetX()) > 0 && (event.clientX - Position.GetX()) < 62 && (event.clientY - Position.GetY()) > 0 && (event.clientY - Position.GetY()) < 10 ){
			this.m_VolumePositionElement.style.width = (event.clientX - Position.GetX()) + 'px'; 
			this.m_VideoController.GetVideoPlayer().GetVideoElement().volume = (this.m_VolumePositionElement.clientWidth * 1) / 62; 

		} else this.EndSeek(); 
		
	}
	
}; 

NDVolumeController.prototype.StartSeek = function( event ){
	this.m_Seeking = true; 
	this.TrackPointer( event );
	
}; 

NDVolumeController.prototype.EndSeek = function(){
	this.m_Seeking = false; 
	
}; 

////////////////////////////////////////////////////////////////////////////////////
// NDScrubController
////////////////////////////////////////////////////////////////////////////////////
function NDScrubController( i_VideoController ){
	Object.defineProperties( this, {
		m_VideoController : {
			value: i_VideoController
			, writable: true
		}
		, m_ContainerElement : {
			value: document.createElement( 'div' )
			, writable: true
		}
		, m_ScrubElement : {
			value: document.createElement( 'div' )
			, writable: true
		}
		, m_BufferPositionElement : {
			value: document.createElement( 'div' )
			, writable: true
		}
		, m_PlayPositionElement : {
			value: document.createElement( 'div' )
			, writable: true
		}
		, m_Seeking : {
			value: false
			, writable: true
		}
		, m_Playing : {
			value: false
			, writable: true
		}
		
	}); 
	
	this.m_BufferPositionElement.setAttribute( 'class', 'BufferPosition' ); 
	this.m_PlayPositionElement.setAttribute( 'class', 'PlayPosition' ); 
	
	this.m_ScrubElement.setAttribute( 'class', 'Scrub' ); 
	this.m_ScrubElement.setAttribute( 'onmousedown', 'VideoPlayer.GetVideoController().GetScrubController().StartSeek(event);' ); 
	this.m_ScrubElement.setAttribute( 'onmouseup', 'VideoPlayer.GetVideoController().GetScrubController().EndSeek();' ); 
	this.m_ScrubElement.setAttribute( 'onmousemove', 'VideoPlayer.GetVideoController().GetScrubController().TrackPointer(event);' ); 
	
	this.m_ScrubElement.appendChild( this.m_BufferPositionElement ); 
	this.m_ScrubElement.appendChild( this.m_PlayPositionElement ); 
	
	this.m_ContainerElement.setAttribute( 'class', 'ScrubContainer' ); 
	this.m_ContainerElement.appendChild( this.m_ScrubElement ); 
	
}

NDScrubController.prototype.constructor = NDScrubController; 

NDScrubController.prototype.GetContainerElement = function(){
	return this.m_ContainerElement; 
}; 

NDScrubController.prototype.UpdateBufferProgress = function(){
	var ProgressPercent = ((this.m_VideoController.GetVideoPlayer().GetVideoElement().buffered.end( null ).toString() * 1) / (this.m_VideoController.GetVideoPlayer().GetVideoElement().duration * 1)) * 100; 
	
	this.m_BufferPositionElement.style.width = ((ProgressPercent * 247) / 100) + 'px'; 
	
	if( ProgressPercent == 100 ){
		clearInterval( BufferProgressInterval ); 
	}
	
}; 

NDScrubController.prototype.UpdatePlayProgress = function(){
	var ProgressPercent = ((this.m_VideoController.GetVideoPlayer().GetVideoElement().currentTime * 1) / (this.m_VideoController.GetVideoPlayer().GetVideoElement().duration * 1)) * 100; 
	
	this.m_PlayPositionElement.style.width = Math.floor( ((ProgressPercent * 247) / 100)) + 'px'; 
	
	if( this.m_VideoController.GetVideoPlayer().GetVideoElement().ended ){
		this.m_VideoController.GetVideoPlayer().GetVideoElement().currentTime = 0; 
		this.m_PlayPositionElement.style.width = 0; 
		this.m_VideoController.GetPlayPauseController().Pause(); 
		this.m_VideoController.Show(); 
		
		clearInterval( PlayProgressInterval ); 
	}
	
}; 

NDScrubController.prototype.GetPointerPosition = function( el ){
	var X = 0; 
	var Y = 0; 
	
	while( el && !isNaN( el.offsetLeft ) ){
		X += el.offsetLeft - el.scrollLeft; 
		Y += el.offsetTop - el.scrollTop; 
		
		el = el.offsetParent; 
	}
	
	return new NDPosition( X, Y ); 
	
}; 

NDScrubController.prototype.TrackPointer = function( event ){
	if( this.m_Seeking ){
		var Position = this.GetPointerPosition( this.m_ScrubElement ); 
		
		if( (event.clientX - Position.GetX()) > 0 && (event.clientX - Position.GetX()) < 247 && (event.clientY - Position.GetY()) > 0 && (event.clientY - Position.GetY()) < 10 ){
			this.m_PlayPositionElement.style.width = (event.clientX - Position.GetX()) + 'px'; 
			this.m_VideoController.GetVideoPlayer().GetVideoElement().currentTime = ((this.m_PlayPositionElement.clientWidth * 1) * (this.m_VideoController.GetVideoPlayer().GetVideoElement().duration * 1)) / 247; 

		} else this.EndSeek(); 
		
	}
	
	
		
}; 

NDScrubController.prototype.StartSeek = function( event ){
	if( !this.m_VideoController.GetVideoPlayer().GetVideoElement().paused ){
		this.m_VideoController.GetPlayPauseController().Pause(); 
		this.m_Playing = true; 
		
	} else {
		this.m_VideoController.GetPlayPauseController().Pause(); 
		this.m_Playing = false; 
		
	}
	
	this.m_Seeking = true; 
	this.TrackPointer( event );
	
}; 

NDScrubController.prototype.EndSeek = function(){
	if( this.m_Playing )
		this.m_VideoController.GetPlayPauseController().Play(); 
	
	this.m_Seeking = false; 
	
}; 

////////////////////////////////////////////////////////////////////////////////////
// NDPlayPauseController
////////////////////////////////////////////////////////////////////////////////////
function NDPlayPauseController( i_VideoController ){
	Object.defineProperties( this, {
		m_VideoController : {
			value: i_VideoController
			, writable: true
		}
		, m_ContainerElement : {
			value: document.createElement( 'div' )
			, writable: true
		}
		, m_ImageElement : {
			value: document.createElement( 'img' )
			, writable: true
		}
		
	}); 
	
	this.m_ImageElement.setAttribute( 'src', '/NDVideoPlayer/images/play.png' ); 
	
	this.m_ContainerElement.setAttribute( 'class', 'PlayPause' );
	this.m_ContainerElement.setAttribute( 'onmousedown', 'VideoPlayer.GetVideoController().GetPlayPauseController().ButtonDown();' ); 
	this.m_ContainerElement.setAttribute( 'onmouseup', 'VideoPlayer.GetVideoController().GetPlayPauseController().ButtonUp();' ); 
	this.m_ContainerElement.setAttribute( 'onmouseout', 'VideoPlayer.GetVideoController().GetPlayPauseController().ButtonUp();' ); 
	this.m_ContainerElement.setAttribute( 'onclick', 'VideoPlayer.GetVideoController().GetPlayPauseController().Play();' ); 
	
	this.m_ContainerElement.appendChild( this.m_ImageElement ); 
	
}

NDPlayPauseController.prototype.constructor = NDPlayPauseController; 

NDPlayPauseController.prototype.GetContainerElement = function(){
	return this.m_ContainerElement; 
}; 

NDPlayPauseController.prototype.Play = function(){
	BufferProgressInterval = setInterval( 'VideoPlayer.GetVideoController().GetScrubController().UpdateBufferProgress()', 500 ); 
	PlayProgressInterval = setInterval( 'VideoPlayer.GetVideoController().GetScrubController().UpdatePlayProgress()', 100 ); 
	
	this.m_ContainerElement.setAttribute( 'onclick', 'VideoPlayer.GetVideoController().GetPlayPauseController().Pause();' ); 
	this.m_ImageElement.setAttribute( 'src', '/NDVideoPlayer/images/pause.png' ); 
	
	this.m_VideoController.GetVideoPlayer().GetVideoElement().play(); 
	
}; 

NDPlayPauseController.prototype.Pause = function(){
	this.m_ContainerElement.setAttribute( 'onclick', 'VideoPlayer.GetVideoController().GetPlayPauseController().Play();' ); 
	this.m_ImageElement.setAttribute( 'src', '/NDVideoPlayer/images/play.png' ); 
	
	this.m_VideoController.GetVideoPlayer().GetVideoElement().pause(); 
		
}; 

NDPlayPauseController.prototype.ButtonDown = function(){
	this.m_ContainerElement.setAttribute( 'style', 'background: -webkit-gradient(linear, left top, left bottom, from(#aaa), to(#ddd)); background: -moz-linear-gradient(top,  #aaa,  #ddd);' ); 
}; 

NDPlayPauseController.prototype.ButtonUp = function(){
	this.m_ContainerElement.removeAttribute( 'style' ); 
}; 

////////////////////////////////////////////////////////////////////////////////////
// NDVideoController
////////////////////////////////////////////////////////////////////////////////////
function NDVideoController( i_VideoPlayer ){
	Object.defineProperties( this, {
		m_VideoPlayer : {
			value: i_VideoPlayer
			, writable: true
		}
		, m_ContainerElement : {
			value: document.createElement( 'div' )
			, writable: true
		}
		, m_PlayPauseController : {
			value: new NDPlayPauseController( this )
			, writable: true
		}
		, m_ScrubController : {
			value: new NDScrubController( this )
			, writable: true
		}
		, m_VolumeController : {
			value: new NDVolumeController( this )
			, writable: true
		}
		
	}); 
	
	this.m_ContainerElement.setAttribute( 'class', 'NDVideoController' );
	this.m_ContainerElement.setAttribute( 'onmouseover', 'VideoPlayer.GetVideoController().Show();' ); 
	this.m_ContainerElement.style.marginTop = (this.m_VideoPlayer.GetSize().GetHeight() - 80) + 'px'; 
	
	this.m_ContainerElement.appendChild( this.m_PlayPauseController.GetContainerElement() ); 
	this.m_ContainerElement.appendChild( this.m_ScrubController.GetContainerElement() );
	this.m_ContainerElement.appendChild( this.m_VolumeController.GetContainerElement() );
	
}

NDVideoController.prototype.constructor = NDVideoController; 

NDVideoController.prototype.GetContainerElement = function(){
	return this.m_ContainerElement; 
}; 

NDVideoController.prototype.GetVideoPlayer = function(){
	return this.m_VideoPlayer; 
}; 

NDVideoController.prototype.GetPlayPauseController = function(){
	return this.m_PlayPauseController; 
}; 

NDVideoController.prototype.GetScrubController = function(){
	return this.m_ScrubController; 
}; 

NDVideoController.prototype.GetVolumeController = function(){
	return this.m_VolumeController; 
}; 

NDVideoController.prototype.Hide = function(){ 
	if( !this.m_VideoPlayer.GetVideoElement().paused ){
		if( ControllerOpacityTimeout ) clearTimeout( ControllerOpacityTimeout ); 
		
		if( ControllerOpacity > 0.0 ){
			ControllerOpacity -= 0.2;
			this.m_ContainerElement.style.opacity = ControllerOpacity;
			
			ControllerOpacityTimeout = setTimeout( 'VideoPlayer.GetVideoController().Hide()', 50 );
			
		} else {
			ControllerOpacity = 0.0; 
			this.m_ContainerElement.style.opacity = ControllerOpacity;
			
			clearTimeout( ControllerOpacityTimeout ); 
			
		}
		
	} else {
		this.Show();  
	}

}; 

NDVideoController.prototype.Show = function(){
	if( ControllerOpacityTimeout ) clearTimeout( ControllerOpacityTimeout ); 
	
	if( ControllerOpacity < 1.0 ){
		ControllerOpacity += 0.2;
		this.m_ContainerElement.style.opacity = ControllerOpacity;
		
		ControllerOpacityTimeout = setTimeout( 'VideoPlayer.GetVideoController().Show()', 50 );
		
	} else {
		ControllerOpacity = 1.0; 
		this.m_ContainerElement.style.opacity = ControllerOpacity;
		
		clearTimeout( ControllerOpacityTimeout ); 
		
	}
	 
}; 

////////////////////////////////////////////////////////////////////////////////////
// NDVideoPlayer
////////////////////////////////////////////////////////////////////////////////////
function NDVideoPlayer( i_ParentElement, i_VideoSource, i_Size ){
	Object.defineProperties( this, {
		m_ParentElement : {
			value: i_ParentElement
			, writable: true
		}
		, m_ContainerElement : {
			value: document.createElement( 'div' )
			, writable: false
		}
		, m_Size : {
			value: (i_Size && i_Size instanceof NDSize) ? i_Size : new NDSize( _PLAYER_DEFAULT_WIDTH_, _PLAYER_DEFAULT_HEIGHT_ )
			, writable: true
		}
		, m_VideoElement : {
			value : document.createElement( 'video' )
			, writable: true
		}
		, m_VideoController : {
			value: null
			, writable: true
		}
				
	}); 
	
	this.m_ContainerElement.setAttribute( 'class', 'NDVideoPlayer' ); 
	this.m_ContainerElement.setAttribute( 'style', 'width: '+ this.m_Size.GetWidth() +'px; height: '+ this.m_Size.GetHeight() +'px;' );
	
	this.m_VideoElement.setAttribute( 'style', 'width: '+ this.m_Size.GetWidth() +'px; height: '+ this.m_Size.GetHeight() +'px;' ); 
	this.m_VideoElement.setAttribute( 'onmouseout', 'VideoPlayer.GetVideoController().Hide()' ); 
	this.m_VideoElement.setAttribute( 'onmouseover', 'VideoPlayer.GetVideoController().Show()' ); 
	
	for( i in i_VideoSource ){
		var Source = document.createElement( 'source' ); 
		
		Source.setAttribute( 'src', i_VideoSource[i].GetLocation() ); 
		Source.setAttribute( 'type', i_VideoSource[i].GetType() );
		
		this.m_VideoElement.appendChild( Source ); 
		
	}
	
	this.m_VideoElement.addEventListener( 'click', function(){
		if( this.paused )
			VideoPlayer.GetVideoController().GetPlayPauseController().Play();
		else VideoPlayer.GetVideoController().GetPlayPauseController().Pause(); 
		
	}, false ); 
	
	this.m_ContainerElement.appendChild( this.m_VideoElement ); 
	
	this.m_VideoController = new NDVideoController( this );
	this.m_ContainerElement.appendChild( this.m_VideoController.GetContainerElement() );
	
	this.m_ParentElement.appendChild( this.m_ContainerElement ); 
	
}

NDVideoPlayer.prototype.constructor = NDVideoPlayer; 

NDVideoPlayer.prototype.GetVideoController = function(){
	return this.m_VideoController; 
}; 

NDVideoPlayer.prototype.GetContainerElement = function(){
	return this.m_ContainerElement; 
}; 

NDVideoPlayer.prototype.GetVideoElement = function(){
	return this.m_VideoElement; 
}; 

NDVideoPlayer.prototype.SetSize = function( i_Size ){
	this.m_Size = (i_Size && i_Size instanceof NDSize) ? i_Size : new NDSize( _PLAYER_DEFAULT_WIDTH_, _PLAYER_DEFAULT_HEIGHT_ ); 
}; 

NDVideoPlayer.prototype.GetSize = function(){
	return this.m_Size; 
}; 

////////////////////////////////////////////////////////////////////////////////////
// NDVideoPlayerMain
////////////////////////////////////////////////////////////////////////////////////
function NDVideoPlayerMain( i_ParentElement, i_VideoSources, i_Size ){
	VideoPlayer = new NDVideoPlayer( i_ParentElement, i_VideoSources, i_Size ); 
	
}
